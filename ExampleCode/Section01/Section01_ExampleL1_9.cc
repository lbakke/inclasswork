/*************************************
 * File name: Section01_ExampleL1_9.cc  
 * Author: Matthew Morrison
 * Email: matt.morrison@nd.edu 
 * 
 * Given a set of inputs from the user, demonstrates
 * min, max, and stack operations 
 * ***********************************/

#include <iostream>
#include <cstdlib>
#include "max_stack.h"

const int INPUT_THROW = 100;

/******************************
 * Function Name: clearSTDCIN
 * Preconditions: none 
 * Postconditions: none
 * 
 * Clears the std::cin buffer in the event 
 * that the user wishes to after an error.
 * ****************************/
void clearSTDCIN(){
	std::cin.clear();
    std::cin.ignore(256, '\n');
}

/******************************
 * Function Name: getInputs
 * Preconditions: none 
 * Postconditions: none
 * 
 * Clears the std::cin buffer in the event 
 * that the user wishes to after an error.
 * ****************************/
void getInputs(size_t* stack_size, size_t* val_max){

    std::cout << "Enter the values as [size] [range]: ";

    std::cin >> *stack_size;
        
    if(!std::cin.good()){
        std::cerr << "Input " << stack_size << " is incorrect. Must be an integer." << std::endl;
        
        // Throw to the catch block in main 
        throw INPUT_THROW;
    }
    
    std::cin >> *val_max;
        
    if(!std::cin.good()){
        std::cerr << "Input " << val_max << " is incorrect. Must be an integer." << std::endl;
        
        // Throw to the catch block in main 
        throw INPUT_THROW;
    }
    
    clearSTDCIN();

 }

int main(int argc, char **argv)
{
    try{   
        size_t stack_size;
        size_t val_max;
        
        getInputs(&stack_size, &val_max);
        
        srand(time(NULL));
        
        max_stack<int>* the_stack = new max_stack<int>();
        
        for(int i = 0; i < stack_size; i++){
            the_stack->push(rand() % val_max);
        }
        
        /*
        the_stack->push(17);
        the_stack->push(10);
        the_stack->push(18);
        the_stack->push(1);
        the_stack->push(25);
        */
        
        std::cout   << "size = " << the_stack->data_size 
                    << ", max_size = " << the_stack->max_size
                    << ", min_size = " << the_stack->min_size << std::endl;
                    
        for(int i = 0; i < stack_size; i++){
            //std::cout << the_stack->top() << '\t' << the_stack->max() << '\t' << the_stack->min();
            the_stack->pop();
            //std::cout << std::endl;
        }
    }catch(int INPUT_THROW){
        std::cout << "Exiting Program..." << std::endl;
        exit(-1);
    }

    return 0;
}
